#! /bin/sh

for in in *.gv
do
    if [ -a $in ]
    then
        out=${in%.gv}.png
        echo "$in -> $out"
        dot -Gdpi=150 -Tpng -Kneato -n -o$out $in
    fi
done
echo "done!"
