#! /bin/sh

lualatex abstract
lualatex avtoreferat
lualatex avtoreferat_booklet
lualatex pz
lualatex rd
lualatex rd_prog
lualatex rd_sys_prog
lualatex review
lualatex tp
lualatex tz_19
lualatex tz_34

pdflatex presentation
lualatex presentation_compact
