<?php

/************************************************************

Запускать из папки preg

Категории:

1_7_7 - hrd
1_7_8 - hrd
2_3_9 - med++
1_4_4 - med+
1_5_4 - med+

2_2_7 - med+ (и заменить вопросы на QTextCodec под Qt 5)
1_5_9 - med
2_2_10 - med
2_4_5 - med
2_4_8 - med

1_7_3 - med-
1_7_6 - med-
2_1_5 - med-
2_2_6 - med-
1_4_8 - eas

1_5_8 - eas
1_6_5 - eas
2_1_3 - eas
2_1_9 - eas
2_3_8 - eas

************************************************************/

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');

require_once('question.php');
require_once('questiontype.php');

function write_csv($filename, $data) {
    $fp = fopen($filename, 'w');
    array_unshift($data, array(/*'category_id',*/ 'category_name', /*'question_id',*/ 'question_name', 'response', /*'best_fit_answer_id',*/ 'full'));
    foreach ($data as $fields) {
        fputcsv($fp, $fields);
    }
    fclose($fp);
}

function data_cmp($a, $b) {
    if ($a['category_name'] !== $b['category_name']) {
        return strcmp($a['category_name'], $b['category_name']);
    }
    if ($a['question_name'] !== $b['question_name']) {
        return strcmp($a['question_name'], $b['question_name']);
    }
    return strcmp($a['response'], $b['response']);
}

function collect_data(&$correct, &$incorrect) {
    global $DB;

    $slow_questions = array(
        '1_7_6_preg_6'
    );

    $qtype = new qtype_preg();

    // Получаем категории
    /*$categories = $DB->get_records_sql("SELECT id, name

    FROM mdl_question_categories

    WHERE name LIKE '%1_7_7%' OR
          name LIKE '%1_7_8%' OR
          name LIKE '%2_3_9%' OR
          name LIKE '%1_4_4%' OR
          name LIKE '%1_5_4%' OR

          name LIKE '%2_2_7%' OR
          name LIKE '%1_5_9%' OR
          name LIKE '%2_2_10%' OR
          name LIKE '%2_4_5%' OR
          name LIKE '%2_4_8%' OR

          name LIKE '%1_7_3%' OR
          name LIKE '%1_7_6%' OR
          name LIKE '%2_1_5%' OR
          name LIKE '%2_2_6%' OR
          name LIKE '%1_4_8%' OR

          name LIKE '%1_5_8%' OR
          name LIKE '%1_6_5%' OR
          name LIKE '%2_1_3%' OR
          name LIKE '%2_1_9%' OR
          name LIKE '%2_3_8%'");*/

    $categories = $DB->get_records_sql("SELECT id, name

    FROM mdl_question_categories

    WHERE name LIKE '%1_5_8%' OR
          name LIKE '%1_5_9%' OR
          name LIKE '%1_6_5%' OR
          name LIKE '%1_7_7%' OR
          name LIKE '%1_7_8%' OR
          name LIKE '%2_1_3%'");

    //$categories = $DB->get_records_sql("SELECT id, name FROM mdl_question_categories WHERE name LIKE '%2_3_8%'");

    // Формируем запрос для получения всех вопросов из этих категорий
    $query = "SELECT * FROM mdl_question WHERE qtype='preg' AND (";
    foreach ($categories as $category) {
        $query .= "category={$category->id}";
        if ($category !== end($categories)) {
            $query .= ' OR ';
        } else {
            $query .= ');';
        }
    }

    // Получаем вопросы
    $questions = $DB->get_records_sql($query);

    $cnt_questions = count($questions);
    $cur_question = 0;

    foreach ($questions as $questionid => $question) {
        $question->contextid = 0;
        $qtype->get_question_options($question);
        $pregquestion = $qtype->make_question($question);

        // Пропускаем зависающие регексы
        if (in_array($pregquestion->name, $slow_questions)) {
            continue;
        }

        // Отключаем генерацию подсказок, они нам не нужны и только тормозят
        $pregquestion->usecharhint = 0;
        $pregquestion->uselexemhint = 0;

        // Получаем непустые попытки ответов на данный вопрос
        // qast.fraction IS NOT NULL AND
        $responses = $DB->get_records_sql("SELECT DISTINCT qastd.value

                                           FROM mdl_question_attempt_step_data qastd
                                           JOIN mdl_question_attempt_steps qast ON qastd.attemptstepid = qast.id
                                           JOIN mdl_question_attempts qa ON qast.questionattemptid = qa.id

                                           WHERE qa.questionid = ? AND
                                                 qastd.value <> '' AND
                                                 qastd.name LIKE '%answer%';", array($questionid));

        $cnt_responses = count($responses);
        echo "new question: name = {$pregquestion->name}, id = $questionid, $cnt_responses responses\n";

        $cur_response = 0;
        foreach ($responses as $responseid => $response) {
            $bestfit = $pregquestion->get_best_fit_answer(array('answer' => $response->value));
            $tmp = array(
                //'category_id' => $question->category,
                'category_name' => $categories[$question->category]->name,
                //'question_id' => $question->id,
                'question_name' => $question->name,
                'response' => $response->value,
                //'best_fit_answer_id' => $bestfit['answer']->id,
                'full' => $bestfit['match']->full
            );
            if ($bestfit['match']->full) {
                $correct[] = $tmp;
            } else {
                $incorrect[] = $tmp;
            }

            ++$cur_response;
            echo "processed $cur_response of $cnt_responses responses\r";
            //echo "\n";
        }

        // Напечатаем прогресс
        ++$cur_question;
        echo "\n";
        echo "processed $cur_question of $cnt_questions questions\n";
        echo "\n";
    }
}

$correct = array();
$incorrect = array();

$timestart = round(microtime(true));

// Собираем данные и пишем в файл
collect_data($correct, $incorrect);

usort($correct, 'data_cmp');
usort($incorrect, 'data_cmp');

write_csv('correct.csv', $correct);
write_csv('incorrect.csv', $incorrect);

$timeend = round(microtime(true));
$seconds = $timeend - $timestart;

$cnt_correct = count($correct);
$cnt_incorrect = count($incorrect);

echo "done: $cnt_correct correct and $cnt_incorrect incorrect responses\n";
echo "time taken: $seconds seconds\n";
