#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QHash>

TemplateHighlighter::TemplateHighlighter(QObject *parent) :
    QSyntaxHighlighter(parent)
{
}

TemplateHighlighter::TemplateHighlighter(QTextDocument *parent) :
    QSyntaxHighlighter(parent)
{
}

void TemplateHighlighter::highlightBlock(const QString &text)
{
    QHash<QString, QTextCharFormat> formats;

    QTextCharFormat templateFormat;
    templateFormat.setFontWeight(QFont::Bold);
    templateFormat.setForeground(Qt::darkMagenta);
    formats.insert("\\(\\?###[^)]+\\)", templateFormat);

    QTextCharFormat spaceFormat;
    //spaceFormat.setFontWeight(QFont::Bold);
    spaceFormat.setForeground(QColor("#ADADDD"));
    formats.insert("(?:\\\\s|\\\\n|\\[(?:\\\\s|\\\\n)+\\])[*+?]*", spaceFormat);

    QTextCharFormat parensFormat;
    //parensFormat.setFontWeight(QFont::Bold);
    parensFormat.setForeground(Qt::red);
    formats.insert("\\\\(?:\\(|\\))", parensFormat);

    for (QHash<QString, QTextCharFormat>::const_iterator iter = formats.constBegin(); iter != formats.constEnd(); ++iter) {
        QRegExp expression(iter.key());
        int index = text.indexOf(expression);
        while (index >= 0) {
            int length = expression.matchedLength();
            setFormat(index, length, iter.value());
            index = text.indexOf(expression, index + length);
        }
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    highlighter(NULL)
{
    ui->setupUi(this);

    highlighter = new TemplateHighlighter(ui->regexEdit->document());

    connect(ui->btnApply, &QPushButton::clicked, this, &MainWindow::btnApply_clicked);
    connect(ui->btnClear, &QPushButton::clicked, this, &MainWindow::btnClear_clicked);
    connect(ui->btnDefault, &QPushButton::clicked, this, &MainWindow::btnDefault_clicked);
    connect(ui->btnFixWhitespaces, &QPushButton::clicked, this, &MainWindow::btnFixWhitespaces_clicked);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::btnApply_clicked()
{
    QTextCursor cursor = ui->regexEdit->textCursor();

    QString regex = ui->regexEdit->toPlainText();
    QString templateName = ui->templateNameEdit->text().trimmed();
    QString param1 = ui->param1Edit->text();
    QString param2 = ui->param2Edit->text();
    QString param3 = ui->param3Edit->text();

    if (templateName.isEmpty()) {
        QMessageBox::warning(this, "Incorrect template name", "Specfy a correct template name!");
        return;
    }

    int start = cursor.selectionStart();
    int end = cursor.selectionEnd();

    QString regexBefore = regex.left(start);
    QString regexSelection = regex.mid(start, end - start);
    QString regexAfter = regex.mid(end);

    // Prepare list of params, replace $$ with regex selection
    QStringList params;
    if (!param1.isEmpty()) {
        params << param1;
    }
    if (!param2.isEmpty()) {
        params << param2;
    }
    if (!param3.isEmpty()) {
        params << param3;
    }
    for (int i = 0; i < params.count(); ++i) {
        if (params[i] == "$$") {
            params[i] = regexSelection;
        }
    }

    // Form the result
    QString result = regexBefore;
    if (params.isEmpty()) {
        // Simple template
        result += "(?###" + templateName + ")";
    } else {
        // Template with params
        result += "(?###" + templateName + "<)";
        result += params.join("(?###,)");
        result += "(?###>)";
    }
    result += regexAfter;

    ui->regexEdit->setText(result);
}

void MainWindow::btnClear_clicked()
{
    ui->templateNameEdit->clear();
    ui->param1Edit->clear();
    ui->param2Edit->clear();
    ui->param3Edit->clear();
}

void MainWindow::btnDefault_clicked()
{
    btnClear_clicked();
    ui->templateNameEdit->setText("parens_req");
    ui->param1Edit->setText("$$");
}

void MainWindow::btnFixWhitespaces_clicked()
{
    QString result = ui->regexEdit->toPlainText();
    //result.replace(QRegExp("\[(?:\\\\n|\\\\r|\\\\t|\\\\s)+\]"), "x");
    result.replace("[\\s\\t\\n\\r]", "\\s");

    ui->regexEdit->setText(result);
}
