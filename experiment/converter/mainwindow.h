#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSyntaxHighlighter>

namespace Ui {
class MainWindow;
}

class TemplateHighlighter : public QSyntaxHighlighter
{
public:
    explicit TemplateHighlighter(QObject *parent);
    explicit TemplateHighlighter(QTextDocument *parent);
protected:
    virtual void highlightBlock(const QString &text);
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    TemplateHighlighter *highlighter;

private slots:
    void btnApply_clicked();
    void btnClear_clicked();
    void btnDefault_clicked();
    void btnFixWhitespaces_clicked();
};

#endif // MAINWINDOW_H
